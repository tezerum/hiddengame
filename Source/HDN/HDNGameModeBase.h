// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HDNGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HDN_API AHDNGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
